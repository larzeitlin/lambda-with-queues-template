import rasterio
import numpy as np
import json
import boto3
import os

sqs_client = boto3.client('sqs')
output_queue_url = os.environ.get('OUTPUT_QUEUE_URL')

def process_cog(cog_url):
    with rasterio.open(cog_url) as src:
        with rasterio.Env(CPL_CURL_VERBOSE=False,
                          CPL_VSIL_CURL_ALLOWED_EXTENSIONS='.tif',
                          GDAL_DISABLE_READDIR_ON_OPEN='YES'):
            pixels = src.read()
            data = np.array(pixels)

            # flatten spatial dimensions
            data = data.reshape(data.shape[0],
                                data.shape[1] * data.shape[2])

            medians = np.nanmedian(data, axis=1)
            stds = np.nanstd(data, axis=1)
            varis = np.nanvar(data, axis=1)
            means = np.nanmean(data, axis=1)

            return {"medians": medians.tolist(),
                    "stds": stds.tolist(),
                    "variances": varis.tolist(),
                    "means": means.tolist()}

def lambda_handler(event, context):
    results = []

    for record in event['Records']:
        message_body = json.loads(record['body'])
        cog_url = message_body.get('cog_url')
        if not cog_url:
            continue

        results.append({"cog_url": cog_url,
                        "stats": process_cog(cog_url)})

    response = sqs_client.send_message(
        QueueUrl=output_queue_url,
        MessageBody=json.dumps({"cog_stats": results})
    )

    return {
        "statusCode": 200,
        "body": json.dumps({"Desination": output_queue_url,
                            "MessageId": response["MessageId"]})
    }
