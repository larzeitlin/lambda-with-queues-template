provider "aws" {
  region = "us-west-2"
}

resource "aws_sqs_queue" "input_queue" {
  name = "request-cog-stats"
}

resource "aws_sqs_queue" "output_queue" {
  name = "cog-stats-results"
}

module "lambda_function" {
  source        = "terraform-aws-modules/lambda/aws"
  function_name = "cog_stats"
  description   = "Returns top level stats for a COG"
  handler       = "logic.lambda_handler"
  runtime       = "python3.8"
  source_path   = "../lambda"
  timeout       = 30
  memory_size   = 2048
  publish       = true
  environment_variables = {
    OUTPUT_QUEUE_URL = aws_sqs_queue.output_queue.url
  }
  depends_on = [
    aws_sqs_queue.input_queue,
    aws_sqs_queue.output_queue
  ]
  event_source_mapping = {
    sqs = {
      batch_size       = 1,
      event_source_arn = aws_sqs_queue.input_queue.arn
    }
  }
  attach_policy_statements = true
  policy_statements = {
    sqs_receive = {
      effect = "Allow",
      actions = [
        "sqs:ReceiveMessage",
        "sqs:DeleteMessage",
        "sqs:GetQueueAttributes"
      ],
      resources = [aws_sqs_queue.input_queue.arn]
    },
    sqs_send = {
      effect = "Allow",
      actions = [
        "sqs:SendMessage"
      ],
      resources = [aws_sqs_queue.output_queue.arn]
    }
  }
  allowed_triggers = {
    sqs = {
      principal  = "sqs.amazonaws.com"
      source_arn = aws_sqs_queue.input_queue.arn
    }
  }
}


