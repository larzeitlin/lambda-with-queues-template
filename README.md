# lambda with queues template
An example terraform / lambda / aws project to set up a lambda that does some processing of cogs with input and output queues. This can be used as a building block for more complex processing pipelines.


## Structure
* `lambda/` contains Python code that is executed in AWS Lambda.
* `terraform/` contains terraform scripts. (Just `main.tf` for now)

## Deployment
### Pre-requisites
* Correct configuration of AWS credentials for the AWS CLI:
https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
* Terraform installed 
https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli

### Deploy infrastructure
Terraform will deploy two SQS Queues, a Lambda function and all the required IAM configuration.

From within the `terraform/`:
1. run `terraform init`.
2. Inspect the plan using `terraform plan`.
3. Deploy the infractructure using `terraform apply`.

### Usage
The Lambda is triggered by the SQS queue called `request-cog-stats`. Add a message to this queue (this can be done via the AWS console for example).

#### Example message body:
```
{"cog_url": "https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/53/S/QV/2023/12/S2A_53SQV_20231206_0_L2A/B04.tif"}
```

### Expected Response:
A successful execution of the lambda will cause the result to be written to the `cog-stats-results` queue. The result body includes the input URL.

```
{
   "cog_stats":[
      {
         "cog_url":"https://sentinel-cogs.s3.us-west-2.amazonaws.com/sentinel-s2-l2a-cogs/53/S/QV/2023/12/S2A_53SQV_20231206_0_L2A/B04.tif",
         "stats":{
            "medians":[
               206.0
            ],
            "stds":[
               654.8659063777021
            ],
            "variances":[
               428849.3553358894
            ],
            "means":[
               355.60442098732256
            ]
         }
      }
   ]
}
```

